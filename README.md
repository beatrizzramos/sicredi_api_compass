# API SICREDI - Simulação de Crédito

## Como executar a API localmente?

- Para iniciar a API localmente, é possível baixá-la clicando em: [Desafio QA - API Sicredi](https://github.com/desafios-qa-automacao/desafio-sicredi).

Na pasta raiz do projeto (\desafio-sicredi\prova-tecnica-api), abra o terminal (CMD, GitBash, etc.) e execute o seguinte comando:


`````mvn clean spring-boot:run`````


- Dessa forma, a API será executada por padrão na porta 8080.

- Após a API estar em execução, você pode acessar a [Documentação Swagger](http://localhost:8080/swagger-ui.html#).

## Como executar os Testes e ver seus Resultados?

- Para rodá-los localmente, faça o download do repositório, abra a pasta principal e execute o seguinte comando:

  `````mvn test -Denv=local`````
  
- Pronto! Agora você consegue analisar todos os testes criados para a API Sicredi - Simulação de Crédito.


##  Agradecimentos
Gostaria expressar minha gratidão à turma do programa de bolsas Java Back-End Automação de Testes da Compass UOL. Onde, diariamente trocamos experiências e partilhamos de conhecimentos. Ademais, agradecer aos mentores, que estão sempre disponíveis para nos ajudar.